package fr_yogj_ardoise_magique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

public class Fenetre_Execution extends JFrame {
	private Dessin pan = new Dessin();
	private JPanel container = new JPanel();
	private JMenuBar menuBar = new JMenuBar();
	private JMenu fichier = new JMenu("Fichier"),
				  edition = new JMenu("Edition"),
				  menuForme = new JMenu("Forme du pointeur"),
				  menuCouleur = new JMenu("Couleur du pointeur");
	private JMenuItem effacer = new JMenuItem("Effacer", 'f'),
					  quitter = new JMenuItem("Quitter",'Q'),
					  carreForme = new JMenuItem("CARRE", 'A'),
					  rondForme = new JMenuItem("ROND",'N'),
					  rougePointeur = new JMenuItem("Rouge",'R' ),
					  bleuPointeur = new JMenuItem("Bleu",'B'),
					  noirPointeur = new JMenuItem("Noir", 'N');
	private JToolBar barreOutil = new JToolBar();
	private JButton squareBouton = new JButton(new ImageIcon("carre.JPG")),
					rondBouton = new JButton(new ImageIcon("rond.JPG")),
					rougeBouton = new JButton(new ImageIcon("carreRouge.JPG")),
					bleuBouton = new JButton(new ImageIcon("carreBleu.JPG")),
					noirBouton = new JButton(new ImageIcon("carre.JPG"));
	private Color fondBouton = Color.WHITE;
	private FormeListener formeListener = new FormeListener();
	private CouleurListener couleurListener = new CouleurListener();
	
/**
 * Constructeur de la fenetre o� s'execute le programme
 * 1 menu et 1 barre d'outil
 */
	public Fenetre_Execution() {
		//Param de la fenetre
		//*******************
		this.setTitle("Ardoise Magique");
		this.setForeground(Color.GRAY);
		//this.setAlwaysOnTop(true);
		this.setExtendedState(this.MAXIMIZED_BOTH);
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Param du container
		//******************
		container.setForeground(Color.WHITE);
		container.setLayout(new BorderLayout());
		container.add(pan, BorderLayout.CENTER);
		//Ajout du menu
		//*************
		this.setContentPane(container);
		this.initMenu();
		this.initToolBar();
		this.setVisible(true);
	}
	/**
	 * methode d'initialisation du menu
	 */
	public void initMenu() {
		//Menu "fichier"
		//**************
		//effacer
		//*******
		effacer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_DOWN_MASK ));
		effacer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				JOptionPane jop = new JOptionPane();
				int option = jop.showConfirmDialog(null, "Voulez-vous effacer votre oeuvre ?",
								"Dession Effac�", 
								JOptionPane.YES_NO_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (option == JOptionPane.OK_OPTION) 
					pan.erase();
			}
		});
		fichier.add(effacer);
		fichier.addSeparator();
		//quitter
		//*******
		quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));
		quitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fichier.add(quitter);
		//Menu "edition"
		//**************
		//menu forme
		//**********
		carreForme.addActionListener(formeListener);
		rondForme.addActionListener(formeListener);
		menuForme.add(carreForme);
		menuForme.add(rondForme);
		menuForme.setMnemonic('F');
		edition.add(menuForme);
		
		edition.addSeparator();
		
		//menu couleur
		//************
		bleuPointeur.addActionListener(couleurListener);
		rougePointeur.addActionListener(couleurListener);
		noirPointeur.addActionListener(couleurListener);
		menuCouleur.add(bleuPointeur);
		menuCouleur.add(rougePointeur);
		menuCouleur.add(noirPointeur);
		menuCouleur.setMnemonic('C');
		edition.add(menuCouleur);
		
		
		//Ajout des menus ds la barre et param des mnemoniques
		//****************************************************
		fichier.setMnemonic('F');
		edition.setMnemonic('E');
		menuBar.add(fichier);
		menuBar.add(edition);
		
		this.setJMenuBar(menuBar);
	}
	/**
	 * methode initialisant la barre d'outil
	 */
	public void initToolBar() {
		//Ajout des boutons
		//*****************
		this.squareBouton.setBackground(fondBouton);
		this.squareBouton.addActionListener(formeListener);
		this.barreOutil.add(squareBouton);
		this.rondBouton.setBackground(fondBouton);
		this.rondBouton.addActionListener(formeListener);
		this.barreOutil.add(rondBouton);
		
		this.barreOutil.addSeparator();
		
		this.noirBouton.setBackground(fondBouton);
		this.noirBouton.addActionListener(couleurListener);
		this.barreOutil.add(noirBouton);
		this.bleuBouton.setBackground(fondBouton);
		this.bleuBouton.addActionListener(couleurListener);
		this.barreOutil.add(bleuBouton);
		this.rougeBouton.setBackground(fondBouton);
		this.rougeBouton.addActionListener(couleurListener);
		this.barreOutil.add(rougeBouton);
		
		this.add(barreOutil, BorderLayout.NORTH);
		
	}
	/**
	 * class interne qui permet le changement de forme du pointeur
	 * @author nicolas
	 *
	 */
	class FormeListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().getClass().getName().equals("javax.swing.JMenuItem")) {
				if(e.getSource()== carreForme)
					pan.setFormePointeur("Carre");
				else 
					pan.setFormePointeur("Rond");
			}
			else {
				if(e.getSource() == squareBouton)
					pan.setFormePointeur("carre");
				else
					pan.setFormePointeur("Rond");
			}
		}
	}
	/**
	 * class interne qui permet le changement de couleur du pointeur
	 * @author nicolas
	 *
	 */
	class CouleurListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(e.getSource().getClass().getName().equals("javax.swing.JMenuItem")) {
				if(e.getSource() == noirPointeur)
					pan.setCouleurPointeur(Color.BLACK);
				if(e.getSource() == bleuPointeur)
					pan.setCouleurPointeur(Color.BLUE);
				if(e.getSource() == rougePointeur)
					pan.setCouleurPointeur(Color.RED);
			}
			else {
				if(e.getSource() == noirBouton)
					pan.setCouleurPointeur(Color.BLACK);
				if(e.getSource() == bleuBouton)
					pan.setCouleurPointeur(Color.BLUE);
				if(e.getSource() == rougeBouton)
					pan.setCouleurPointeur(Color.RED);
			}
		}
	}
	
	
	public static void main(String[] args) {
		Fenetre_Execution f = new Fenetre_Execution();
	}

}
