package fr_yogj_ardoise_magique;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Point extends JPanel {
	private Color couleur = Color.BLACK;
	private String forme = "Rond";
	private int posX = -10; 
	private int posY = -10;
	private int taille = 10;
	/**
	 * Constructeur par defaut
	 */
	public Point() {}
	/**
	 * Constructeur avec parametres
	 * @param pPosX coordonnee X
	 * @param pPosY coordonne Y
	 * @param pTaille taille du point
	 * @param pForme forme du point
	 * @param pCouleur couleur du point
	 */
	public Point(int pPosX, int pPosY, int pTaille, String pForme, Color pCouleur ) {
		this.posX = pPosX;
		this.posY = pPosY;
		this.taille = pTaille;
		this.forme = pForme;
		this.couleur = pCouleur;
	}
	/**
	 * Les mutateurs et accesseurs
	 * 
	 */
	public void setPosX(int pX) {
		this.posX = pX;
	}
	public void setPosY(int pY) {
		this.posY = pY;
	}
	public int getPosX() {
		return posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setTaille(int pTaille) {
		this.taille = pTaille;
	}
	public int getTaille() {
		return this.taille;
	}
	public void setForme(String pForme) {
		this.forme = pForme;
	}
	public String getForme() {
		return this.forme;
	}
	public void setCouleur(Color pColor) {
		this.couleur = pColor;
	}
	public Color getCouleur() {
		return this.couleur;
	}
}
