package fr_yogj_ardoise_magique;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;



public class Dessin extends JPanel {
	private Color couleurPointeur = Color.BLACK;
	private String formePointeur = "Rond";
	private int posXPointeur = -10;
	private int posYPointeur = -10;
	private int taillePointeur = 10;
	private boolean erasing = true;
	private ArrayList<Point> dessin = new ArrayList<Point>();
/**
 * Constructeur
 * on ajoute le listener de la souris pour afficher le dessin
 */
	public Dessin() {
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				dessin.add(new Point(e.getX()-(taillePointeur/2), e.getY()-(taillePointeur/2),taillePointeur,formePointeur, couleurPointeur));
				repaint();
			}
		});
		this.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent e) {
				dessin.add(new Point(e.getX()-(taillePointeur/2), e.getY()-(taillePointeur/2),taillePointeur,formePointeur, couleurPointeur));
				repaint();
			}
			public void mouseMoved(MouseEvent e) {}
		});
	}
	/**
	 * gere l'affichage du dessin selon la couleur et la forme choisie
	 */
	public void paintComponent(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		if(this.erasing)
			this.erasing = false;
		else {
			for(Point p : dessin) {
				g.setColor(p.getCouleur());
				if (p.getForme().equals("Rond"))
					g.fillOval(p.getPosX(), p.getPosY(), p.getTaille(), p.getTaille());
				else
					g.fillRect(p.getPosX(), p.getPosY(), p.getTaille(), p.getTaille());
			}
		}
	}
	/**
	 * Gere l'effacement du dessin
	 */
	public void erase() {
			this.erasing = true;
			this.dessin = new ArrayList<Point>();
			repaint();
	}
	
	public void setCouleurPointeur(Color pCouleur) {
		this.couleurPointeur = pCouleur;
	}
	public void setFormePointeur (String pForme) {
		this.formePointeur = pForme;
	}
}
