package exercice_intro;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

public class Fenetre extends JWindow {
	private JProgressBar jpb;
	private Chargement pan;
	private JPanel panel;
	
	public Fenetre() {
		this.setSize(600,600);
		this.setLocationRelativeTo(null);
		
		this.jpb = new JProgressBar();
		jpb.setMaximum(100);
		jpb.setMinimum(0);
		jpb.setStringPainted(true);
		jpb.setBorder(BorderFactory.createLoweredBevelBorder());
		
		this.pan = new Chargement();
		//this.panel = new JPanel();
		//this.panel.setBackground(Color.WHITE);
		//this.panel.setBorder(BorderFactory.createTitledBorder("LE PENDU"));
		//this.panel.add(pan, BorderLayout.CENTER);
		//this.panel.add(jpb, BorderLayout.SOUTH);
		//this.getContentPane().add(panel);		
		this.getContentPane().add(pan, BorderLayout.CENTER);
		this.getContentPane().add(jpb, BorderLayout.SOUTH);
		
		
		Thread tLoad = new Thread(new ChargementBarre());
		Thread tPendu = new Thread(new ChargementPendu());
		tPendu.start();
		tLoad.start();
		


	}
	
	class ChargementBarre implements Runnable{
		public void run() {
			for (int i = 0; i <= 100; i++) {
				jpb.setValue(i);
				try {
					Thread.sleep(100);
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	class ChargementPendu implements Runnable{
		public void run() {
			pan.peindre(-1);
			for (int i = 0; i <= 9; i++) {
				pan.peindre(i);
				try {
					Thread.sleep(1000);
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	
	
	
	public static void main(String[] args) {
		Fenetre f = new Fenetre();
		
		
		f.setVisible(true);

	}

}
