package exercice_intro;

public interface Observable {
	public void addObservateur(Observateur o);
	public void updateObservateur();
	public void delObservateur();
}

