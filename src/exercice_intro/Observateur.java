package exercice_intro;

public interface Observateur {
	public void update(Chargement load);
}
