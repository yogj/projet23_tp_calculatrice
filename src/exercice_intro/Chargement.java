package exercice_intro;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Chargement extends JPanel {
	private int posX, posY;
	public boolean init = true;
	private int etape = -1;

	public Chargement() {
		posX = 0;
		posY = 0;
	}

	public void peindre(int etape) {
		this.etape = etape;
		this.paint(this.getGraphics());
	}

	public void paintComponent(Graphics g) {
		if (init) {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			this.init = false;
			g.setColor(Color.BLACK);
		}

		switch (etape) {
			case 0: {
				posX = 0;
				posY = 550;
				g.drawLine(posX, posY, posX + 50, posY);
			}
			break;
			
			case 1: {
				posX = posX + 50;
				g.drawLine(posX / 2, posY, posX / 2, (posY - 450));
			}
			break;
				
			case 2: {
				posX = posX / 2;
				posY = posY - 450;
				g.drawLine(posX, posY, posX + 400, posY);
			}
			break;
				
			case 3: {
				posX = posX + 400;
				g.drawLine(posX, posY, posX, posY + 50);
			}
			break;
				
			case 4: {
				posX = posX / 2;
				posY = posY - 450;
				g.drawLine(posX, posY, posX + 400, posY);
			}
			break;
			
			case 5: {
				posX = posX - 25;
				posY = posY + 50;
				g.drawLine(posX, posY, posX, posY + 200);
			}
			break;
			
			case 6: {
				posY = posY + 200;
				g.drawLine(posX, posY, posX + 50, posY + 50);
				g.drawLine(posX - 50, posY + 50, posX - 50, posY + 100);
			}
			break;
			
			case 7: {
				g.drawLine(posX, posY, posX - 50, posY + 50);
				g.drawLine(posX + 50, posY + 50, posX + 50, posY + 100);
			}
			break;
			
			case 8: {
				posY = posY - 150;
				g.drawLine(posX, posY, posX - 50, posY);
			}
			break;
			
			case 9: {
				g.drawLine(posX, posY, posX + 50, posY);
			}
			break;
		}
	}
}
