package fr_yogj_projet52_testeur_requete;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

public class Testeur_requete extends JFrame {

	private static final long serialVersionUID = 1L;
	private JToolBar tool = new JToolBar();
	private JButton bouton = new JButton(new ImageIcon("Ressources/play.JPG"));
	private JSplitPane split;
	private String requete = "SELECT * FROM Classe";
	private JTextArea reqZone = new JTextArea(requete);
	private JPanel result = new JPanel();

	public Testeur_requete(){
		this.setSize(600,600);
		this.setTitle("Testeur de requ�te");
		this.setLocationRelativeTo(null);
		this.setForeground(Color.GRAY);
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		initToolBar();
		initContent();
		initTable(requete);
	}
	
	public void initToolBar() {
		bouton.setPreferredSize(new Dimension (100,50));
		bouton.setBorder(null);
		bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				initTable(reqZone.getText());				
			}
		});
		bouton.add(tool);
		this.getContentPane().add(tool, BorderLayout.NORTH);
	}
	
	public void initContent() {
		result.setLayout(new BorderLayout());
		split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(reqZone), result);
		split.setDividerLocation(200);
		this.getContentPane().add(split, BorderLayout.CENTER);
	}
	
	public void initTable(String pRequete) {
		try {
			//On cree un statement
			long start = System.currentTimeMillis();
			Statement state = bdd_singleton.getInstance().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//On exec la requete
			ResultSet resultat = state.executeQuery(requete);
			 //On r�cup�re les meta afin de r�cup�rer le nom des colonnes
			ResultSetMetaData meta = resultat.getMetaData();
			 //On initialise un tableau d'Object pour les en-t�tes du tableau
			Object[] titre = new Object[meta.getColumnCount()];
			for (int i = 1; i<=meta.getColumnCount(); i++)
				titre[i-1] = meta.getColumnName(i);
			//Petite manipulation pour obtenir le nombre de lignes
			resultat.last();
			int compteur = resultat.getRow();
			Object[][] data = new Object[resultat.getRow()][meta.getColumnCount()];
			//On revient au d�part
			resultat.beforeFirst();
			int j = 1;
			//On remplit le tableau d'Object[][]
			while (resultat.next()) {
				for(int i = 1; i<=meta.getColumnCount(); i++)
					data[j-1][i-1] = resultat.getObject(i);
				j++;
			}
			resultat.close();
			state.close();
			long tempsReq = System.currentTimeMillis() - start ;
			//On enl�ve le contenu de notre conteneur
			result.removeAll();
			//On y ajoute un JTable
			result.add(new JScrollPane(new JTable(data, titre)), BorderLayout.CENTER);
			result.add(new JLabel("Temps d'execution de la requete : "+tempsReq+" ms \n Nb de lignes retourn�es : "+compteur));
			   //On force la mise � jour de l'affichage
			result.revalidate();
				
			
			
		}catch (SQLException e) {
			 //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu
			result.removeAll();
			result.add(new JScrollPane(new JTable()), BorderLayout.SOUTH);
			result.revalidate();
			JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR !", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void main(String[] args) {
		Testeur_requete test = new Testeur_requete();
		test.setVisible(true);
	}

}
