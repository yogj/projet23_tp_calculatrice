package fr_yogj_projet52_testeur_requete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class bdd_singleton {
	private String url = "jdbc:postgresql://localhost:5432/Ecole";//--URL de co
	private String user = "postgres"; //--utilisateur
	private String mdp = "ajx6tix"; //--mot de passe
	
	private static Connection conn;//--notre objet connection
	
	//Constructeur priv�
	private bdd_singleton(){
		try {
			conn = DriverManager.getConnection(url, user, mdp);
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR DE CONNEXION ! ", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static Connection getInstance(){
		if (conn == null) {
			new bdd_singleton();
			System.out.println("INSTANCIATION DE LA CO SQL ! ");
		}
		else {
			System.out.println("CONNEXION DEJA EXISTANTE ! ");
		}
		return conn;
	}
}
