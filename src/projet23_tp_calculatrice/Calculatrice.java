package projet23_tp_calculatrice;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Calculatrice extends JFrame {
	private JPanel container = new JPanel();
	private JLabel ecran;
	private boolean update = false;
	private boolean clicOperateur = false;
	private double valeur;
	private String operateur = "";
	private String[] listeCarac= {"7","8","9","4","5","6","1","2","3","0",".","=","C","+","-","*","/"};
	private JButton[] listeBouton = new JButton[listeCarac.length];
	private Dimension dimBouton = new Dimension (145,128);
	private Dimension dimOperateur = new Dimension(114,101 );
	
	public Calculatrice() {
		this.setTitle("Calculatrice");
		this.setSize(600,650);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		initComposant0();

		this.setContentPane(container);
		this.setVisible(true);
	}
	public void initComposant0() {
		Font police = new Font("Arial", Font.BOLD, 40);
		ecran = new JLabel("0");
		ecran.setFont(police);
		ecran.setForeground(Color.BLACK);
		ecran.setHorizontalAlignment(JLabel.RIGHT);
		ecran.setPreferredSize(new Dimension(550, 60));

		JPanel panelAffichage = new JPanel();
		panelAffichage.setPreferredSize(new Dimension(580, 70));
		panelAffichage.add(ecran);
		panelAffichage.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JPanel panelOperateur = new JPanel();
		panelOperateur.setPreferredSize(new Dimension(130,515));
		JPanel panelChiffre = new JPanel();
		panelChiffre.setPreferredSize(new Dimension(470, 515));
		
		for (int i=0; i<listeCarac.length; i++) {
			listeBouton[i] = new JButton(listeCarac[i]);
			listeBouton[i].setFont(police);
			listeBouton[i].setPreferredSize(dimBouton);
			switch (i) {
			case 10 : 
				listeBouton[i].addActionListener(new BoutonDecimalListener());
				panelChiffre.add(listeBouton[i]);
				break;
			case 11 : 
				listeBouton[i].addActionListener(new BoutonEgalListener());
				panelChiffre.add(listeBouton[i]);
				break;
			case 12 : 
				listeBouton[i].setForeground(Color.RED);
				listeBouton[i].setPreferredSize(dimOperateur);
				listeBouton[i].addActionListener(new BoutonCListener());
				panelOperateur.add(listeBouton[i]);
				break;
			case 13 : 
				listeBouton[i].addActionListener(new BoutonPlusListener());
				listeBouton[i].setPreferredSize(dimOperateur);
				panelOperateur.add(listeBouton[i]);
				break;
			case 14 :
				listeBouton[i].addActionListener(new BoutonMoinsListener());
				listeBouton[i].setPreferredSize(dimOperateur);
				panelOperateur.add(listeBouton[i]);
				break;
			case 15 : 
				listeBouton[i].addActionListener(new BoutonMultiplicationListener());
				listeBouton[i].setPreferredSize(dimOperateur);
				panelOperateur.add(listeBouton[i]);
				break;
			case 16 : 
				listeBouton[i].addActionListener(new BoutonDivisionListener());
				listeBouton[i].setPreferredSize(dimOperateur);
				panelOperateur.add(listeBouton[i]);
				break;
			default : 
				listeBouton[i].addActionListener(new BoutonChiffreListener());
				listeBouton[i].setPreferredSize(dimBouton);
				panelChiffre.add(listeBouton[i]);
				break;
			}
			container.setBackground(Color.WHITE);
			container.setLayout(new BorderLayout());
			container.add(panelAffichage, BorderLayout.NORTH);
			container.add(panelChiffre, BorderLayout.CENTER);
			container.add(panelOperateur, BorderLayout.EAST);
		}
	}
	
	private void calcul() {
		if  (operateur.equals("+")) {
			valeur = valeur + Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(valeur));
		}
		if (operateur.equals("-")) {
			valeur = valeur - Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(valeur));
		}	
		if (operateur.equals("*")) {
			valeur = valeur * Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(valeur));
		}
		if (operateur.equals("/")) {
			try {
			valeur = valeur / Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(valeur));
			}catch (ArithmeticException e) {
				ecran.setText("-E-");
			}
		}
	}
	
	class BoutonChiffreListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String str = ((JButton)e.getSource()).getText();
			if(update) { 
				update = false;
			}
			else {
				if (!ecran.getText().equals("0")) {
					str = ecran.getText() + str;
				}
			}
			ecran.setText(str);	
		}
	}
	class BoutonMoinsListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (clicOperateur) {
				calcul();
				ecran.setText(String.valueOf(valeur));
			}
			else {
				valeur = Double.valueOf(ecran.getText()).doubleValue();
				clicOperateur = true;
			}
			operateur = "-";
			update = true;
			listeBouton[10].setEnabled(true);
		}
	}
	class BoutonPlusListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (clicOperateur) {
				calcul();
				ecran.setText(String.valueOf(valeur));
			}
			else {
				valeur = Double.valueOf(ecran.getText()).doubleValue();
				clicOperateur = true;
			}
			operateur = "+";
			update = true;
			listeBouton[10].setEnabled(true);
		}
	}
	class BoutonDivisionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (clicOperateur) {
				calcul();
				ecran.setText(String.valueOf(valeur));
			}
			else {
				valeur = Double.valueOf(ecran.getText()).doubleValue();
				clicOperateur = true;
			}
			operateur = "/";
			update = true;
			listeBouton[10].setEnabled(true);
		}
	}
	class BoutonMultiplicationListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (clicOperateur) {
				calcul();
				ecran.setText(String.valueOf(valeur));
			}
			else {
				valeur = Double.valueOf(ecran.getText()).doubleValue();
				clicOperateur = true;
			}
			operateur = "*";
			update = true;
			listeBouton[10].setEnabled(true);
		}
	}
	class BoutonCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			clicOperateur = false;
			update = true;
			valeur = 0d;
			operateur = "";
			ecran.setText("");
			listeBouton[10].setEnabled(true);
		}
	}
	class BoutonDecimalListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String str = ((JButton)e.getSource()).getText();
			if (update) {	
				update = false;
			}
			else {
				if(!ecran.getText().equals("0")) {
					str = ecran.getText()+str;
				}
			}
			ecran.setText(str);
			listeBouton[10].setEnabled(false);
		}
	}
	class BoutonEgalListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			calcul();
			update = true;
			clicOperateur = false;
			listeBouton[10].setEnabled(true);
		}
	}
	public static void main(String[] args) {
		Calculatrice calc = new Calculatrice();

	}

}
