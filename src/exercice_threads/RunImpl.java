package exercice_threads;

import java.util.ArrayList;

public class RunImpl implements Runnable, Observable, Observateur{

	private Fenetre_execution fenetre;
	private String role = "slave";
	private ArrayList<Observateur> listObs = new ArrayList<Observateur>();
	private int compteurEmis, compteurRecu;
	
	/**
	 * Constructeur avec param
	 * @param f une fenetre JFrame
	 * @param pRole le role du bot
	 */
	public RunImpl(Fenetre_execution f, String pRole) {
		this.fenetre = f;
		this.role = pRole;
		fenetre.setVisible(true);
	}
	/**
	 * le dialogue entre les thread est gere ici selon le role
	 */
	public void run() {
		while(true) {
			//Master
			//******
			if(role.equals("master")) {
				try {
					Thread.sleep(5000);
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
				//On reinitialise les compteurs
				//*****************************
				if (compteurEmis == 5) {
					compteurRecu = 0;
					compteurEmis = 0;
				}
				//Qd il a recu autant qu'il a emis, il informe l'obs et affiche le compteurEmis
				//*****************************************************************************
				if (compteurEmis == compteurRecu) {
					this.updateObservateur();
					fenetre.setEcran1(">>> Envoye : "+String.valueOf(compteurEmis)+" ");
				}
				//il affiche le compteurRecu
				//**************************
				fenetre.setEcran2("<<< Recu : "+String.valueOf(compteurRecu)+" ");
			}
			//Slave
			//*****
			else { 
				try {
					Thread.sleep(1000);
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
				//Affiche le compteurRecu, informe l'obs et affiche le compteurEmis
				//*****************************************************************
				if (compteurEmis < compteurRecu) {
					fenetre.setEcran1("<<< Recu : "+String.valueOf(compteurRecu)+" ");
					compteurEmis = compteurRecu;
					try {
						Thread.sleep(2000);
					}catch (InterruptedException e) {
						e.printStackTrace();
					}
					updateObservateur();
					fenetre.setEcran2(">>> Envoye : "+String.valueOf(compteurEmis)+" ");
					//On reinitialise les compteurs
					//*****************************
					if (compteurRecu == 5) {
						compteurEmis = 0;
						compteurRecu = 0;
					}
				}
			}
		}
	}
	/**
	 * met a jour le compteurRecu selon le role du bot
	 */
	public void update (int pValeur) {
		if (role.equals("master"))
			this.compteurRecu = pValeur;
		else {
			this.compteurRecu = pValeur;
			System.out.println(compteurRecu);
		}
	}
	/**
	 * gestion de la liste des abonnes 
	 */
	public void updateObservateur() {
		for (Observateur o : this.listObs) {
			if (role.equals("master")) {
				compteurEmis ++;
				o.update(compteurEmis);
			}
			else
				o.update(this.compteurEmis);
		}

	}
	public void addObservateur(Observateur o) {
		this.listObs.add(o);
	}
	public void delObservateur() {
		this.listObs = new ArrayList<Observateur>();
	}
}
