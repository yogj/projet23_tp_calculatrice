package exercice_threads;

public interface Observateur {
	public void update(int pValeur);
}
