package exercice_threads;

public interface Observable {
	public void addObservateur(Observateur o);
	public void updateObservateur();
	public void delObservateur();
}
