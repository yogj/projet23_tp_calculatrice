package exercice_threads;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class Fenetre_execution extends JFrame {
	private JLabel ecran1 = new JLabel();
	private JLabel ecran2 = new JLabel();
	
	
	/**
	 * constructeur avec param
	 * @param pTitre
	 * @param pX coordonnee X de la fenetre
	 * @param pY coordonnee Y de la fenetre
	 */
	public Fenetre_execution(String pTitre, int pX, int pY) {
		//param fenetre
		//************* 
		this.setTitle(pTitre);
		this.setSize(600,600);
		this.setForeground(Color.GRAY);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(pX, pY);
		// param etiquettes
		// ****************
		Font police = new Font("Arial", Font.BOLD, 16);
		ecran1 = new JLabel();
		this.ecran1.setFont(police);
		this.ecran1.setPreferredSize(new Dimension(200,200));
		this.ecran1.setHorizontalAlignment(JLabel.LEFT);
		ecran1.setText(" - ");
		
		ecran2 = new JLabel();
		this.ecran2.setFont(police);
		this.ecran2.setPreferredSize(new Dimension(200,200));
		this.ecran2.setHorizontalAlignment(JLabel.RIGHT);
		ecran2.setText(" - ");

		this.getContentPane().add(ecran1, BorderLayout.NORTH);
		this.getContentPane().add(ecran2, BorderLayout.SOUTH);
	}
	/**
	 * Gere ce qui est affiche sur l'ecran1 : le message
	 * @param message
	 */
	public void setEcran1(String message) {
		this.ecran1.setText(message);
	}
	/**
	 * Gere ce qui est affiche sur l'ecran2 : le message
	 * @param message
	 */
	public void setEcran2(String message) {
		this.ecran2.setText(message);
	}
	/**
	 * lancement du prog
	 * @param args
	 */
	public static void main(String[] args) {
		//On construit deux fenetres
		//**************************
		Fenetre_execution f1 = new Fenetre_execution("Bot1", 200, 500);
		Fenetre_execution f2 = new Fenetre_execution("Bot2", 650, 500);
		//On leur implemente l'interface Runnable
		//***************************************
		RunImpl r1 = new RunImpl(f1, "master");
		RunImpl r2 = new RunImpl(f2, "slave");
		//On leur ajoute les ecouteurs
		//****************************
		r1.addObservateur(r2);
		r2.addObservateur(r1);
		//On cree et on lance les processus
		//*********************************
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		t1.start();
		t2.start();
		

	}

}
