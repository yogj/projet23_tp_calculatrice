package fr_yogj_table_jeu_pendu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fenetre extends JFrame{
	private JPanel panNord, panSud, panCentre;
	private Clavier clavier;
	private String mot = "aa";
	private String motCache;
	private char[] listLettre;

	public Fenetre() {
		//Param de la fenetre
		//*******************
		this.setTitle("Table de jeu du pendu");
		this.setForeground(Color.GRAY);
		this.setExtendedState(this.MAXIMIZED_BOTH);
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Panel Nord contient les 3 etiquettes nomJoueur, nbMotTrouve, scoreActuel
		//************************************************************************
		this.panNord = new JPanel();
		this.panNord.setLayout(new BoxLayout(panNord, BoxLayout.PAGE_AXIS));
		Font police = new Font("Arial", Font.BOLD, 18);
		
		//Etiquette nomJoueur
		//*******************
		JLabel nomJoueur = new JLabel();
		nomJoueur.setFont(police);
		nomJoueur.setPreferredSize(new Dimension(300,100));
		nomJoueur.setForeground(Color.BLACK);
		nomJoueur.setHorizontalAlignment(JLabel.CENTER);
		//nomJoueur.setText(this.joueur.getNom()+" tente sa chance !");
		panNord.add(nomJoueur);
		
		//Etiquette nbMotTrouve
		//*********************
		JLabel nbMotTrouve = new JLabel();
		nbMotTrouve.setFont(police);
		nbMotTrouve.setPreferredSize(new Dimension(300,100));
		nbMotTrouve.setForeground(Color.BLACK);
		nbMotTrouve.setHorizontalAlignment(JLabel.CENTER);	
		//nbMotTrouve.setText("Nombres de mots trouv�s : "+(String).valueOf(this.joueur.getNbMot()));
		panNord.add(nbMotTrouve);
		
		//Etiquette score actuel
		//**********************
		JLabel scoreActuel = new JLabel();
		scoreActuel.setFont(police);
		scoreActuel.setPreferredSize(new Dimension(300,100));
		scoreActuel.setForeground(Color.BLACK);
		scoreActuel.setHorizontalAlignment(JLabel.CENTER);	
		//scoreActuel.setText("Votre score actuel : "+(String).valueOf(this.joueur.getScore()));
		panNord.add(scoreActuel);
		
		//Panel Sud : le mot a decouvrir
		//******************************
		this.panSud = new JPanel();
			
		Font policeMot = new Font("Arial", Font.BOLD, 36);
		JLabel motCacheLbl = new JLabel();
		motCacheLbl.setFont(policeMot);
		motCacheLbl.setPreferredSize(new Dimension(300,100));
		motCacheLbl.setForeground(Color.BLUE);
		motCacheLbl.setHorizontalAlignment(JLabel.CENTER);
		motCacheLbl.setBorder(BorderFactory.createRaisedBevelBorder());
		//On initialise le mot a decouvrir et on affiche des etoiles ds l'etiquette 
		//*************************************************************************
		this.setMotCache();
		motCacheLbl.setText(motCache);
		//motCache.addObservateur
		panSud.add(motCacheLbl);
		
		//Panel Image qui contient l'image du pendu
		//*****************************************
		JPanel panImage = new JPanel();
		JLabel etqImage = new JLabel(new ImageIcon("imgVierge.JPG"));
		etqImage.setPreferredSize(new Dimension(350,350));
		etqImage.setHorizontalTextPosition(JLabel.CENTER);
		panImage.add(etqImage);
		
		//Panel clavier qui contient le clavier
		//*************************************
		JPanel panKey = new JPanel();
		panKey.setPreferredSize(new Dimension (1000, 350));
		this.clavier = new Clavier();
		panKey.setBorder(BorderFactory.createLoweredBevelBorder());
		panKey.add(clavier);
		
		//Panel central contenant le clavier et l'image
		//*********************************************
		this.panCentre = new JPanel();
		panCentre.setLayout(new FlowLayout());
		panCentre.add(panKey);
		panCentre.add(panImage);
				
		//Panel contenant tous les autres
		//*******************************
		this.getContentPane().add(panNord, BorderLayout.NORTH);
		this.getContentPane().add(panSud, BorderLayout.SOUTH);
		this.getContentPane().add(panCentre, BorderLayout.CENTER);
		
	}
	
	
	public void setMotCache() {
		int nbre = (int)(Math.random()*336529);
		//lecture du fichier dictionnaire jusqu'a la ligne nbre
		//on ecrit le mot choisi ds notre variable mot
		
		//this.mot = ???;
		//On cree une liste de lettre avec les lettres du mot
		//***************************************************
		for (int i = 0 ; i<mot.length() - 1; i++)
			listLettre[i] = mot.charAt(i);
		
		
		
		
		//On cree le mot cache qui sera affiche ds l'etiquette
		//****************************************************
		this.motCache = "*";
		for(int i = 0; i <this.mot.length() - 1; i++) 
			motCache += "*";
		
	}
	
	
	
	
	
	
	
	public static void main(String[] args) {
		Fenetre f = new Fenetre();
		f.setVisible(true);

	}

}
