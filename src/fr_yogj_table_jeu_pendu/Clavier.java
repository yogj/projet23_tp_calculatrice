package fr_yogj_table_jeu_pendu;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class Clavier extends JPanel {
	private JButton[] listBouton;
	private String[] listChar = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	private JPanel panBouton;
	private String lettre;
	
	public Clavier() {
		//Le panel qui accueillera les boutons selon un rangement sur grille gl
		//*********************************************************************
		this.panBouton = new JPanel();
		GridLayout gl = new GridLayout(3,9);
		gl .setHgap(7);
		gl.setVgap(7);
		this.panBouton.setLayout(gl);
		//On cree nos boutons dans une liste
		//**********************************
		Font police = new Font("Arial", Font.BOLD, 25);
		this.listBouton = new JButton[this.listChar.length];
		//On construit nos boutons dans la liste
		//**************************************
		for(int i = 0; i <listChar.length; i++) {
			listBouton[i] = new JButton(this.listChar[i]);
			listBouton[i].setFont(police);
			listBouton[i].setPreferredSize(new Dimension(100,100));
			//On ajoute les ecouteurs sur les boutons
			//***************************************
			listBouton[i].addKeyListener(new KeyListener(){

				@Override
				public void keyPressed(KeyEvent e) {
					//String str = (String).valueOf(e.getKeyChar());
				}

				@Override
				public void keyReleased(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}
				
			});
			listBouton[i].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					lettre = ((JButton)e.getSource()).getText();
				}
			});
			//On ajoute les boutons au panel
			//******************************
			panBouton.add(listBouton[i]);
		}
		//On ajout le panel � l'objet
		//***************************
		this.add(panBouton);
	}
	
	public String getLettre() {
		return this.lettre;
	}
}
